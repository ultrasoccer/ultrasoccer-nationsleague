#!/bin/bash

## add envvars to server env
echo "export APACHE_RUN_USER=user"                >> /etc/apache2/envvars
echo "export APACHE_RUN_GROUP=user"               >> /etc/apache2/envvars
echo "export MYSQL_HOST=${MYSQL_HOST}"                >> /etc/apache2/envvars
echo "export MYSQL_PORT=${MYSQL_PORT}"                >> /etc/apache2/envvars
echo "export MYSQL_USER=${MYSQL_USER}"                >> /etc/apache2/envvars
echo "export MYSQL_PASSWORD=${MYSQL_PASSWORD}"        >> /etc/apache2/envvars
echo "export MYSQL_DATABASE=${MYSQL_DATABASE}"        >> /etc/apache2/envvars
echo "export AES_KEY=${AES_KEY}"                      >> /etc/apache2/envvars

## start server
/etc/init.d/apache2 restart

## keep docker container open
sleep inf
