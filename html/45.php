---
layout: default
saison: 45
date: '2023-07-26'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: jp
startliste:
  -
    - kz
    - jp
    - co
    - pl
    - is
    - fr
    - nl
    - de
    - se
    - bw
    - tz

startliste_partien_pause:
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 8
---
