---
layout: default
saison: 37
date: '2022-10-21'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - is
    - es
    - bw
    - nl
    - cm
    - se
    - co
    - pl
    - tz

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
