---
layout: default
saison: 58
date: '2024-10-20'
ligaspieltag: 2
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: fo # nl, fo = 3 / au, ca, gh = 1 => Stichwahl 4 fo / 3 nl
startliste:
  -
    - en
    - kz
    - fo
    - co
    - is 
    - nl
    - se
    - pl
    - tz
    - fr
    - ca
    - gh
    - au
    - kr
startliste_partien_pause:
  - 1
  - 2
  - 2
  - 2
  - 2
  - 1
  - 3
  - 1
  - 2
  - 2
  - 2 
  - 2
  - 0
---
