---
layout: default
saison: 62
date: '2025-03-09'
ligaspieltag: 2
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: ''
startliste:
  -
    - ar
    - is
    - gh
    - nl
    - au
    - kr
    - co
    - bo
    - ca
    - pl
    - fo
    - tz
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
---
