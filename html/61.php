---
layout: default
saison: 61
date: '2025-02-02'
ligaspieltag: 2
step: 1
finals_step: 1
finalteams: 8
managervoting: 
  enabled: false
  result: fo # fo 4 tz 3 se 1 ca 1 
startliste:
  -
    - kz
    - se
    - is
    - gh
    - pl
    - ar
    - fo
    - nl
    - co
    - ca
    - fr
    - au
    - tz
    - kr
    - bo
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 1
  - 1
  - 1
---
