---
layout: default
saison: 30
date: '2022-02-19'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 4
startliste:
  -
    - is
    - hr
    - es
    - bw
    - fr
    - hu
    - pl
    - co
    - cm
    - se

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 4
  - 2
  - 3
---
