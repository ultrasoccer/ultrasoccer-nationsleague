---
layout: default
saison: 51
date: '2024-02-23'
ligaspieltag: 7
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: se # 2 Deutschland und Schweden, 1 USA, Finnland, Irland, Tanzania / Stichwahl 5 Schweden / 2 Deutschland
startliste:
  -
    - us
    - fi
    - co
    - kz
    - is
    - se
    - nl
    - pl
    - fr
    - tz
    - de
    - ie
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
