---
layout: default
saison: 39
date: '2022-12-27'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - bw
    - tz
    - es
    - is
    - de
    - pl
    - nl
    - cm
    - se
    - co
    - br

startliste_partien_pause:
  - 2
  - 4
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
  - 0
---
