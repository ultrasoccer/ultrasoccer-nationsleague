---
layout: default
saison: 41
date: '2023-03-08'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - br
    - es
    - is
    - bw
    - tz
    - nl
    - de
    - se
    - pl 
    - co
    - fr 
    - jp

startliste_partien_pause:
  - 2
  - 4
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 3
  - 0
  - 0
---
