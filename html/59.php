---
layout: default
saison: 59
date: '2024-11-23'
ligaspieltag: 1
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: kr # kr 3 au ca gh 2
startliste:
  -
    - en
    - se
    - kz
    - fr
    - pl 
    - co
    - au
    - is
    - gh
    - nl
    - tz
    - fo
    - ca
    - kr
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 0
---
