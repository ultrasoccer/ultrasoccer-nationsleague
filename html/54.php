---
layout: default
saison: 54
date: '2024-06-04'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: gh # gh 4 / ca tz fr co pl 1
startliste:
  -
    - kr
    - se
    - fo
    - kz 
    - nl 
    - co 
    - us
    - fr
    - is
    - tz
    - pl
    - gh
    - ca
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 0
---
