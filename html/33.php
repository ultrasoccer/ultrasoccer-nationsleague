---
layout: default
saison: 33
date: '2022-05-29'
ligaspieltag: 3
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - fi
    - es
    - sx
    - nl
    - pl
    - is
    - hr
    - se
    - fr
    - bw
    - cm
    - co
    - cy

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 3
  - 1
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
