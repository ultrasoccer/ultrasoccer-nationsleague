---
layout: default
saison: 47
date: '2023-10-09'
ligaspieltag: 10
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: is
startliste:
  -
    - pl
    - co
    - fr
    - kz
    - jp
    - is
    - de
    - bw 
    - se
    - tz
    - nl

startliste_partien_pause:
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 0
---
