---
layout: default
saison: 42
date: '2023-04-12'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: se
startliste:
  -
    - fr
    - jp
    - br
    - bw
    - se
    - is
    - nl
    - es
    - pl 
    - tz
    - de 
    - co

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
