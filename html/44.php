---
layout: default
saison: 44
date: '2023-06-21'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: bw  # bw = 5 ,tz und kz = 1
startliste:
  -
    - jp
    - pl
    - bw
    - fr
    - de
    - se
    - co
    - tz
    - nl
    - is
    - kz

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
