---
layout: default
saison: 38
date: '2022-11-25'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - se
    - tz
    - is
    - es
    - bw
    - pl
    - nl
    - co
    - cm
    - de

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
