---
layout: default
saison: 34
date: '2022-07-03'
ligaspieltag: 3
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - cy
    - sx
    - fi
    - se
    - es
    - pl
    - is
    - bw
    - fr
    - nl
    - co
    - cm
    - hr

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
