---
layout: default
saison: 40
date: '2023-01-31'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - tz
    - es
    - se
    - br
    - bw
    - is
    - de
    - nl
    - pl
    - cm
    - co

startliste_partien_pause:
  - 2
  - 4
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
  - 0
---
