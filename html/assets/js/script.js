---
---
const DAY=24*3600*1000;

var Grps=[];

var _e = (el) => ( document.createElement(el) );

var TTTT = [];
function calculatePartien(saison,date){

  $('#partien').append('<h2 class="saison s_'+saison+'">Saison '+saison+'</h2>');

  // #######################################################################
  // Erstelle Partien der Runde 1 ( Hauptrunde ) / Spiel Jeder gegen Jeden
  var runde=1;

  date=new Date(date).getTime();
  var steps=0;
  var done=[];

  // stelle Gruppen zusammen
  STARTLISTE.forEach(
    function(g){
      var G=[];
      g.forEach(
        function(v){
            //G.push( TEAMS.filter( (a) => (a.nt == v) )[0] );
            G.push( TEAMS.filter( (a) => (a.nt == v) ).map((a) => ( [ Object.fromEntries( Object.entries(a).filter( (b) => ( ! ( b[0] < 1000 ) )) ), a[saison] ] ))[0] ); 
        }
      );
      Grps.push(G);
    }
  );

  var h3= _e('h3');
  h3.classList = 'runde s_'+saison+' r_'+runde;
  h3.innerHTML = 'Runde '+runde+ ' (Hauptrunde)';
  $('#partien').append(h3);
  
  // erzeuge die Partien und prüfe, ob diese bereits gespielt wurden
  STARTLISTE_PARTIEN.forEach(
    function(v,i){
      done.push( grpPartien(date,steps,saison,runde,i+1,v.slice(1)) ); steps+=v[0];
    }
  );

  var AFdone = false;

  // ##################################################################################
  // Erstelle Viertelfinale ( Finalrunde ) / KO-Spiele
  if( FINALTEAMS == 8 ){
    if( done.reduce( (a,b) => a && b ) == true ){
      var Tabelle = calculatePunktestand( saison, runde, false );
    } else {
      var Tabelle = [];
      STARTLISTE.forEach(
        function(w,g){
          for( var i = 0; i < FINALTEAMS / STARTLISTE.length; i++ ){
            Tabelle.push( [
              {
                'kontinent' : 'dummy',
                'nt': 'xx',
                'name': '<i>Gruppe ' + (g+1) + '.'+ (i+1) + '</i>',
                'link': '',
                'manager': 'dummy',
                'start': 27
              }
            , undefined ]);
          }
        }
      );
    }
  } else if ( FINALTEAMS > 8 ){
    if( AFdone ){
    } else {
      var Tabelle = [];
      [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16].forEach(
        function(i){
          Tabelle.push( [
            {
              'kontinent' : 'dummy',
              'nt': 'xx',
              'name': '<i>AF ' + (i) + '</i>',
              'link': '',
              'manager': 'dummy',
              'start': 27
            }
          , undefined ] );
        }
      );
    }
  }

  if( FINALTEAMS >= 8 ){
    Grps = [];
    Grps.push( [ Tabelle.shift(), Tabelle.shift(), Tabelle.shift(), Tabelle.shift(), Tabelle.shift() , Tabelle.shift() , Tabelle.shift() , Tabelle.shift() ] );

    runde='VF';
    var h3 = _e('h3');
    h3.classList = 'runde s_'+saison+' r_'+runde;
    h3.innerText = 'Viertelfinale';
    $('#partien').append(h3);
    steps+=FINALS_STEP;
    var done = [];
    done.push( grpPartien( date,steps,saison,runde, 1, [ 8,1, 7,2, 6,3, 5,4 ] ) );
    if( done[0].filter((a) => (a == false )).length == 0 ){
      var VFdone = true;
      var Tabelle = done[0];
    } 
  }

  // ##################################################################################
  // Erstelle Halbfinale ( Finalrunde ) / KO-Spiele
  if( FINALTEAMS == 4 ){
    if( done.reduce( (a,b) => a && b ) == true ){
      var Tabelle = calculatePunktestand( saison, runde, false );
    } else {
      var Tabelle = [];
      STARTLISTE.forEach(
        function(w,g){
          for( var i = 0; i < FINALTEAMS / STARTLISTE.length; i++ ){
            Tabelle.push( [
              {
                'kontinent' : 'dummy',
                'nt': 'xx',
                'name': '<i>Gruppe ' + (g+1) + '.'+ (i+1) + '</i>',
                'link': '',
                'manager': 'dummy',
                'start': 27
              }
            ,undefined ]);
          }
        }
      );
    }
  } else if ( FINALTEAMS > 4 ){
    
    if( VFdone ){
    } else {
      var Tabelle = [];
      [1,2,3,4,5,6,7,8].forEach(
        function(i){
          Tabelle.push([
            {
              'kontinent' : 'dummy',
              'nt': 'xx',
              'name': '<i>VF ' + (i) + '</i>',
              'link': '',
              'manager': 'dummy',
              'start': 27
            }
          ,undefined ] );
        }
      );
    }
  }

  if( FINALTEAMS >= 4 ){
    Grps = [];
    Grps.push( [ Tabelle.shift(), Tabelle.shift(), Tabelle.shift(), Tabelle.shift() ] );

    runde='HF';
    var h3 = _e('h3');
    h3.classList = 'runde s_'+saison+' r_'+runde;
    h3.innerText = 'Halbfinale';
    $('#partien').append(h3);
    steps+=FINALS_STEP;
    var done = [];
    done.push( grpPartien( date,steps,saison,runde, 1, [ 1,4, 2,3 ] ) );
    if( done[0].filter((a) => (a == false )).length == 0 ){
      var HFdone = true;
      var Tabelle = done[0];
    } 
  }

  // ##################################################################################
  // Erstelle Finale ( Finalrunde ) / KO-Spiele
  if( FINALTEAMS == 2 ){
    if( done.reduce( (a,b) => a && b ) == true ){
      var Tabelle = calculatePunktestand( saison, runde, false );
    } else {
      var Tabelle = [];
      STARTLISTE.forEach(
        function(w,g){
          for( var i = 0; i < FINALTEAMS / STARTLISTE.length; i++ ){
            Tabelle.push([
              {
                'kontinent' : 'dummy',
                'nt': 'xx',
                'name': '<i>Gruppe ' + (g+1) + '.'+ (i+1) + '</i>',
                'link': '',
                'manager': 'dummy',
                'start': 27
              }
            ,undefined ]);
          }
        }
      );
    }
  } else if ( FINALTEAMS > 2 ){
    if( HFdone ){
    } else {
      var Tabelle = [];
      [1,2].forEach(
        function(i){
          Tabelle.push([
            {
              'kontinent' : 'dummy',
              'nt': 'xx',
              'name': '<i>HF ' + (i) + '</i>',
              'link': '',
              'manager': 'dummy',
              'start': 27
            }
          ,undefined ]);
        }
      );
    }
  }

  if( FINALTEAMS >= 2 ){
    Grps = [];
    Grps.push( [ Tabelle.shift(), Tabelle.shift() ] );

    runde='F';
    var h3 = _e('h3');
    h3.classList = 'runde s_'+saison+' r_'+runde;
    h3.innerText = 'Finale';
    $('#partien').append(h3);
    steps+=FINALS_STEP;
    var done = [];
    done.push( grpPartien( date,steps,saison,runde, 1, [ 1,2 ] ) );
    if( done[0].filter((a) => (a == false )).length == 0 ){
      var Fdone = true;
      var Tabelle = done[0];
    } 
  }

}

var St=0;
var PARTIEN = [];

function grpPartien( date,steps,saison,runde,spieltag,Bg ){

  if( Grps.map( (a) => ( a.filter((b) => (b == undefined )).length > 0 ) )[0] ){
    alert( 'Error: Einige Teams existieren nicht in der DB' );
    return;
  }
  
  var done = ( runde == 'VF' || runde == 'HF' || runde == 'F' ) ? [] : false;

  var h4 = _e('h4');
  h4.classList = 'spieltag s_'+saison+' r_'+runde+' t_'+spieltag;
  h4.id = 'r'+runde+'st'+spieltag;
  //$(h4).attr( 'ondblclick', 'getCsv('+saison+',\''+runde+'\','+spieltag+')' );
  h4.innerHTML = '<a class="unformated" href="javascript:getCsv('+saison+',\''+runde+'\','+spieltag+')">Spieltag ' + spieltag + '</a>';
  $('#partien').append(h4);
  var tbl = _e('table');
  tbl.classList = 'partien s_'+saison+' r_'+runde+' t_'+spieltag;
  var partie=0;

  Grps.forEach(
    function(v,i){
      var BG = JSON.parse(JSON.stringify(Bg));
      do {
        
        var h1 = BG.shift()-1;
        var a1 = BG.shift()-1;
        partie++;

        if( h1 == -1 || a1 == -1 ) return;
        var ergebnis=ERGEBNISSE.filter( (a) => ( a.saison == saison && a.runde == runde && a.spieltag == spieltag && a.H.toLowerCase() == v[h1][0].nt.toLowerCase() && a.A.toLowerCase() == v[a1][0].nt.toLowerCase() ) );

        if( v[h1][0].id != undefined ) PARTIEN.push({ 'T' : new Date(date).toJSON().slice(0,10), 'runde' : runde, 'spieltag' : spieltag, 'H' : v[h1][0].id, 'H_nt' : v[h1][0].nt, 'A' : v[a1][0].id, 'A_nt' : v[a1][0].nt, 'MID' : ergebnis.length == 1 ? ergebnis[0].MID : 0 });

        if( ergebnis.length == 1 ){

          ergebnis=ergebnis[0];
          var th=(( ergebnis.MID > 0 ) ? '<a href="https://ultrasoccer.de/match/?id='+ergebnis.MID+'" target="ultrasoccer">'+ergebnis.TH+'</a>' : ergebnis.TH );
          var ta=(( ergebnis.MID > 0 ) ? '<a href="https://ultrasoccer.de/match/?id='+ergebnis.MID+'" target="ultrasoccer">'+ergebnis.TA+'</a>' : ergebnis.TA );
          var dt=new Date(ergebnis.T).toJSON().slice(0,10);
          var st=STAT_SAISON.filter( (a) => ( a.MID == ergebnis.MID )).length == 12;
          ergebnis['partie']=partie;
          if( St != -1 ) St=spieltag;
          if( runde == 'VF' || runde == 'HF' || runde == 'F' ){
            //done.push( th > ta ? TEAMS.filter((a) => (a.nt == ergebnis.H ))[0] : TEAMS.filter((a) => (a.nt == ergebnis.A ))[0] );
            done.push( th > ta ? Object.values(TEAMS).filter( (a) => ( ( a.nt == ergebnis.H  ) ) ).map((a) => ( [ Object.fromEntries( Object.entries(a).filter( (b) => ( ! ( b[0] < 1000 ) )) ), a[saison] ] ))[0] : Object.values(TEAMS).filter( (a) => ( ( a.nt == ergebnis.A  ) ) ).map((a) => ( [ Object.fromEntries( Object.entries(a).filter( (b) => ( ! ( b[0] < 1000 ) )) ), a[saison] ] ))[0] );
          } else {
            done = true;
          }

        } else {
          if( St > 0 ){
            var anker=St;
            setTimeout( function(){ window.location.href='#r'+runde+'st'+anker; },500 );
            St=-1;
          }
          var th='-'; var ta='-'; var dt='<i>'+new Date(date+DAY*steps).toJSON().slice(0,10)+'</i> <sup>'+(steps+LIGASPIELTAG)+'</sup>'; var st=false; var match='';
          if( runde == 'VF' || runde == 'HF' || runde == 'F' ){
            done.push(false);
          } else {
            done = false;
          }
        }

        var tr = _e('tr');
        tr.classList = 'begegnungen ' + (( STARTLISTE.length > 1 ) ? 'gruppe' + i : '' ) + ' h_'+v[h1][0].nt+' a_'+v[a1][0].nt+' '+(( st ) ? 'stats' : '');
        var td = _e('td');
        td.innerHTML = dt;
        tr.append(td);
        
        var td = _e('td');
        $(td).attr('data-id',v[h1][0].nt);
        var sup = _e('sup');
        sup.innerHTML = v[h1][0].nt.toUpperCase();
        td.append(sup);

        var span = _e('span')
        span.innerHTML = ' '+v[h1][0].name.replace(/\(NL\)/g,'')+' ';
        td.append(span);

        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( v[h1][0].flag ) ? v[h1][0].flag : v[h1][0].nt.toLowerCase())+'.svg';
        td.append(img);
        tr.append(td);
        
        var td = _e('td');
        td.innerText = ' - ';
        tr.append(td);

        var td = _e('td');
        $(td).attr('data-id',v[a1][0].nt);
        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( v[a1][0].flag ) ? v[a1][0].flag : v[a1][0].nt.toLowerCase())+'.svg';
        td.append(img);

        var span = _e('span')
        span.innerHTML = ' '+v[a1][0].name.replace(/\(NL\)/g,'')+' ';
        td.append(span);

        var sup = _e('sup');
        sup.innerHTML = v[a1][0].nt.toUpperCase();
        td.append(sup);
        tr.append(td);

        var td = _e('td');
        $(td).attr('data-mid',(( ergebnis.MID != undefined ) ? ergebnis.MID : '0' ) ); 
        td.innerHTML = th;
        tr.append(td);
        
        var td = _e('td');
        $(td).attr('onmouseenter', "showLastResults('"+v[h1][0].nt+"','"+v[a1][0].nt+"')" );
        $(td).attr('onmouseleave', 'hideLastResults()');
        $(td).attr('onclick','ignoreHideLastResults()');
        td.innerText = ':';
        tr.append(td);
        
        var td = _e('td');
        td.innerHTML = ta;
        tr.append(td);

        tbl.append(tr);
      } while( BG.length > 0 );
    },spieltag
  );

  $('#partien').append(tbl);

  return done;
}

function getCsv(saison, runde, spieltag){
  var tbl=$('.partien.s_'+saison+'.r_'+runde+'.t_'+spieltag)[0];
  var csv=
  Object.values(tbl.rows).map(
    function(v,i){
      var T=[];
      T.push( runde );
      T.push( spieltag );
      T.push( v.cells[0].innerText.slice(0,10) );
      T.push( v.cells[1].dataset.id );
      T.push( v.cells[3].dataset.id );
      T.push( v.cells[4].innerText );
      T.push( v.cells[6].innerText );
      T.push( v.cells[4].dataset.mid );
      return T;
    }
  ).join('\n');
  $('#copypaste').toggleClass('d-none');
  $('#copypaste textarea').text('runde,spieltag,T,H,A,TH,TA,MID\n'+csv);
}

function calculateEwigePunktestand(){
  var Ew = {}; 
  ERGEBNISSE.filter( (a) => ( a.runde == 1 ) ).forEach(
    function( v ){
      var H = v.H.toLowerCase();
      var A = v.A.toLowerCase();
      if( Ew[H] == undefined ) Ew[H] = { 'S':0, 'P':0, 'Tp':0, 'Tn':0, 'Zp':0, 'Zn':0, 'Tr':[] }
      Ew[H].S++;
      Ew[H].P += v.TH*1 > v.TA*1 ? 3 : v.TH*1 == v.TA*1 ? 1 : 0;
      Ew[H].Tr.push( v.TH*1 > v.TA*1 ? 'S' : v.TH*1 == v.TA*1 ? 'U' : 'N' );
      Ew[H].Tp += v.TH*1;
      Ew[H].Tn += v.TA*1;
      if( Ew[ v.A ] == undefined ) Ew[A] = { 'S':0, 'P':0, 'Tp':0, 'Tn':0, 'Zp':0, 'Zn':0, 'Tr':[] }
      Ew[A].S++;
      Ew[A].P += v.TA*1 > v.TH*1 ? 3 : v.TH*1 == v.TA*1 ? 1 : 0;
      Ew[A].Tp += v.TA*1;
      Ew[A].Tn += v.TH*1;
      Ew[A].Tr.push( v.TA*1 > v.TH*1 ? 'S' : v.TH*1 == v.TA*1 ? 'U' : 'N' );
    }
  );
  
  var tabelle='#tabelle_P tbody';
  $(tabelle).html('');

  var saison = 0;
  var M = calculateAllPunktestand().map((a) => ( a[0][0].nt ));

  Object.entries(Ew)
    .map( (a) => ( [ TEAMS.filter((b) => (b.nt == a[0] ))[0], a[1] ] ) )
    .sort( (a,b) => ( a[1].P < b[1].P || a[1].P == b[1].P && (a[1].Tp-a[1].Tn) < (b[1].Tp-b[1].Tn) ? 1 : -1 ))
    .forEach(
    function(v,i){
      if( v[0].nt == 'xx' ) return;
      v[1].Zwk = Object.values( v[0] ).filter((a) =>( a.Zwk != undefined )).map((a) => (a.Zwk)).reduce((a,b) => (a+b));
      v[1].Td = v[1].Tp - v[1].Tn;

      var border=SAISON_SETTINGS[saison - 26] == i+1 ? 'dashed ' : '';
      
      var tr = _e('tr');
      if( STARTLISTE.length > 1 ) tr.classList = 'gruppe' + j;
      
      var td = _e('td');
      td.classList = border;
      td.innerText = (i+1);
      tr.append(td);
      
      var t=v[1].Tr.map( (a) => ( a != 'X' ) ? 1 : 0 );
      var trophy=ERGEBNISSE.filter((a) => ( a.saison==SAISON-1 && a.runde=='F')).map((a) => ( a.TH > a.TA ? a.H : a.A ))[0];
      trophy = trophy == undefined ? '' : trophy;
      //var master=TabelleLast.length > 0 ? TabelleLast[0][0].nt : '';
      var master = '';

      if( t.length == 0 ) t.push(0);

      var S=t.reduce( (a,b) => a+b );
      var pokal=ERGEBNISSE.filter((a) => ( ( a.saison*1 < saison || saison == 0 ) && a.runde=='F' && ( parseInt(a.TH) > parseInt(a.TA) && a.H.toLowerCase() == v[0].nt.toLowerCase() || parseInt(a.TH) < parseInt(a.TA) && a.A.toLowerCase() == v[0].nt.toLowerCase() ) ) ).length;
      var finalist=ERGEBNISSE.filter((a) => ( ( a.saison*1 < saison || saison == 0 ) && a.runde=='F' && ( parseInt(a.TH) < parseInt(a.TA) && a.H.toLowerCase() == v[0].nt.toLowerCase() || parseInt(a.TH) > parseInt(a.TA) && a.A.toLowerCase() == v[0].nt.toLowerCase() ) ) ).length;
      var meistertitel=M.filter( (a,i) => ( a == v[0].nt && ( i < saison - 26 || saison == 0 ) ) ).length;
      var managervoted=Object.values(MANAGERVOTING_WINNER).filter((a) => (a == v[0].nt)).length;

      var td = _e('td');
      td.classList = border;
      td.innerHTML = S;
      tr.append(td);

      var td = _e('td');
      td.classList = border;
      if( MANAGERVOTING ){
        var _i = _e('i');
        _i.classList = 'fas fa-thumbs-up ';
        $(_i).attr('onclick', "teamVoting('"+v[0].nt+"')");
        if( SAISON_TEAMVOTED.reduce((a,b) => (a.concat(b))).indexOf(v[0].nt) > -1 ){
          _i.classList += 'done';
          _i.title = 'Manager dieses Teams hat sein Voting abgegeben!';
        } else {
          _i.title = 'Manager dieses Teams hat noch kein Voting abgegeben!';
        }
        td.append(_i);
      }

      var img = _e('img');
      img.classList = 'flags';
      img.src = '/images/flags/'+(( v[0].flag ) ? v[0].flag : v[0].nt.toLowerCase())+'.svg';
      td.append(img);

      var span = _e('span');
      span.innerText = ' ';
      td.append(span);

      var _a = _e('a');
      _a.href = "javascript:calculateTeam("+((saison == 0 && false) ? SAISON_NOW : saison)+",'"+v[0].nt+"\')";
      _a.innerHTML = v[0].name.replace(/\(NL\)/g,'<sup>'+v[0].nt.toUpperCase()+'</sup></a>');
      td.append(_a);

      var _a = _e('a');
      _a.href = v[0].link;
      _a.target = 'ultrasoccer';

      var _i = _e('i');
      _i.classList = 'fas fa-link';
      _a.append(_i);
      td.append(_a);

      if( trophy.toLowerCase() == v[0].nt.toLowerCase() ){
        var _i = _e('i');
        _i.classList = 'fas fa-trophy';
        _i.title = 'Amtierender Titelgewinner';
        td.append(_i);
      }

      if( master.toLowerCase() == v[0].nt.toLowerCase() ){
        var _i = _e('i');
        _i.classList = 'fas fa-crown';
        _i.title = 'Amtierender Meister';
        td.append(_i);
      }

      if(  v[0].nt.toLowerCase() == MANAGERVOTING_TEAM ){
        var _i = _e('i');
        _i.classList = 'managervoted fas fa-thumbs-up';
        _i.title = 'Team der Saison, gewählt von den Managern';
        td.append(_i);
      }
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = ((pokal>0) ? pokal : '');
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = ((meistertitel>0) ? meistertitel : '');
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = ((finalist>0) ? finalist : '');
      tr.append(td);

      var td = _e('td');
      td.classList = border;
      td.innerText = ((managervoted>0) ? managervoted : '');
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = v[1].Tr.map( (a) => ( ( a == 'S') ? 1 : 0 ) ).reduce( (a,b) => a+b );
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = v[1].Tr.map( (a) => ( ( a == 'U') ? 1 : 0 ) ).reduce( (a,b) => a+b );
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = v[1].Tr.map( (a) => ( ( a == 'N') ? 1 : 0 ) ).reduce( (a,b) => a+b );
      tr.append(td);

      var td = _e('td');
      td.classList = border;
      //td.innerHTML = v[1].Tr.map( (a) => '<'+a+'></'+a+'>' ).slice(-6).join('');
      td.innerText = '';
      tr.append(td);

      var td = _e('td');
      td.classList = border;
      td.innerText = v[1].Tp+'/'+v[1].Tn;
      tr.append(td);
      
      var td = _e('td');
      td.classList = border + ' ' + ((v[1].Td > 0 ) ? 'p' : (v[1].Td == 0 ) ? 'u' : 'n');
      td.innerText = ((v[1].Td > 0 ) ? '+' : '') + v[1].Td;
      tr.append(td);
      
      var td = _e('td');
      td.classList = border + ' ' + ((v[1].Zwk > 0 ) ? 'p' : (v[1].Zwk == 0 ) ? 'u' : 'n');
      td.innerText = (v[1].Zwk > 0 ? '+' : '')+(v[1].Zwk / S).toFixed(2);
      tr.append(td);
      
      var td = _e('td');
      td.classList = border;
      td.innerText = v[1].P;
      tr.append(td);

      $(tabelle).append(tr);
    }
  );

}


function calculatePunktestand(saison,runde,show=true){

  $.each( TEAMS,
    function( i,v ){
      TEAMS[i][saison] = { P: 0, Tp: 0, Tn: 0, Td: 0, Tr: ['X'], Zwk: 0, active: (( TEAMS[i].start <= saison )) }
    }
  );

  ERGEBNISSE.filter( (a) => ( ( a.saison == saison || saison == 0 ) && $.isNumeric(a.runde) && a.runde <= runde  ) ).forEach(
    function( v,i ){
      var H=TEAMS.filter( (a) => ( a.nt == v.H.toLowerCase() && ( saison == 0 || a.start*1 <= saison && ( a.ende*1 >= saison || a.ende == 0 ) ) ) );
      var A=TEAMS.filter( (a) => ( a.nt == v.A.toLowerCase() && ( saison == 0 || a.start*1 <= saison && ( a.ende*1 >= saison || a.ende == 0 ) ) ) );
      if( H.length == 0 ){
        alert('Error: Team mit Code '+v.H+' fehlt!!' );
        return;
      }
      if( A.length == 0 ){
        alert('Error: Team mit Code '+v.A+' fehlt!!' );
        return;
      }
      H=H[0][saison];
      A=A[0][saison];
      if( v.TH == '-' || v.TA == '-' ) return;
      if( parseInt(v.TH) > parseInt(v.TA) ){
        H.P+=3;
        H.Tr.push('S');
        A.Tr.push('N');
      } else if( parseInt(v.TH) < parseInt(v.TA) ){
        A.P+=3;
        H.Tr.push('N');
        A.Tr.push('S');
      } else {
        H.P+=1;
        A.P+=1;
        H.Tr.push('U');
        A.Tr.push('U');
      }
      H.Tp+=parseInt(v.TH); H.Tn+=parseInt(v.TA); H.Td=H.Tp-H.Tn;
      A.Tp+=parseInt(v.TA); A.Tn+=parseInt(v.TH); A.Td=A.Tp-A.Tn;
      STAT_ALL_MID.filter((b) => ( b.MID == v.MID && v.H == b.nt )).forEach(
        function(c){
          H.Zwk+=c.Zwk*1;
        }
      );
      STAT_ALL_MID.filter((b) => ( b.MID == v.MID && v.A == b.nt )).forEach(
        function(c){
          A.Zwk+=c.Zwk*1;
        }
      );
    }
  );

  var startliste=STARTLISTE.reduce((a,b)=>(a));

  var Tabelle=
  Object.values(TEAMS)
    .filter( (a) => ( ( a.start <= saison || saison == 0 ) && ( startliste.indexOf( a.nt ) > -2 || ( saison == 0 && a.nt != 'xx' ) ) ) )
    .map((a) => ( [ Object.fromEntries( Object.entries(a).filter( (b) => ( ! ( b[0] < 1000 ) )) ), a[saison] ] ))
    .sort(
      function(a,b){
        return a[1].P < b[1].P ||
        ( a[1].P == b[1].P && a[1].Td < b[1].Td ) ||
        ( a[1].P == b[1].P && a[1].Td == b[1].Td && a[1].Tp < b[1].Tp ) ||
        ( a[1].P == b[1].P && a[1].Td == b[1].Td && a[1].Tp == b[1].Tp && a.nt > b.nt ) ? 1 : -1;
      }
    )
    .filter(
      function(b){
        if( ! b[1].active && saison != 0 ) return false;
        if( b[1].P == 0 && b[1].Tp == 0 && b[1].Tn == 0 ) return false;
        return true;
      } 
    );

  if( !show ){
    return Tabelle;
  }

  var tabelle='#tabelle_P tbody';
  $(tabelle).html('');

  var TabelleLast= ( saison > 26 ) ? calculateAllPunktestand()[saison-27] : [];
  var M = calculateAllPunktestand().map((a) => ( a[0][0].nt ));

  STARTLISTE.forEach(
    function(w,j){

      Tabelle.filter((a) => ( w.indexOf(a[0].nt) > -1 )).forEach(
        function(v,i){

          var border=SAISON_SETTINGS[saison - 26] == i+1 ? 'dashed ' : '';
  
          var tr = _e('tr');
          if( STARTLISTE.length > 1 ) tr.classList = 'gruppe' + j;
          
          var td = _e('td');
          td.classList = border;
          td.innerText = (i+1);
          tr.append(td);
          
          var t=v[1].Tr.map( (a) => ( a != 'X' ) ? 1 : 0 );
          var trophy=ERGEBNISSE.filter((a) => ( a.saison==SAISON-1 && a.runde=='F')).map((a) => ( a.TH > a.TA ? a.H : a.A ))[0];
          trophy = trophy == undefined ? '' : trophy;
          var master=TabelleLast.length > 0 ? TabelleLast[0][0].nt : '';
          
          if( t.length == 0 ) t.push(0);
    
          var S=t.reduce( (a,b) => a+b );
          var pokal=ERGEBNISSE.filter((a) => ( ( a.saison*1 < saison || saison == 0 ) && a.runde=='F' && ( parseInt(a.TH) > parseInt(a.TA) && a.H.toLowerCase() == v[0].nt.toLowerCase() || parseInt(a.TH) < parseInt(a.TA) && a.A.toLowerCase() == v[0].nt.toLowerCase() ) ) ).length;
          var finalist=ERGEBNISSE.filter((a) => ( ( a.saison*1 < saison || saison == 0 ) && a.runde=='F' && ( parseInt(a.TH) < parseInt(a.TA) && a.H.toLowerCase() == v[0].nt.toLowerCase() || parseInt(a.TH) > parseInt(a.TA) && a.A.toLowerCase() == v[0].nt.toLowerCase() ) ) ).length;
          var meistertitel=M.filter( (a,i) => ( a == v[0].nt && ( i < saison - 26 || saison == 0 ) ) ).length;
          var managervoted=Object.values(MANAGERVOTING_WINNER).filter((a) => (a == v[0].nt)).length;
    
          var td = _e('td');
          td.classList = border;
          td.innerHTML = S;
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          if( MANAGERVOTING ){
            var _i = _e('i');
            _i.classList = 'fas fa-thumbs-up ';
            $(_i).attr('onclick', "teamVoting('"+v[0].nt+"')");
            if( SAISON_TEAMVOTED.reduce((a,b) => (a.concat(b))).indexOf(v[0].nt) > -1 ){
              _i.classList += 'done';
              _i.title = 'Manager dieses Teams hat sein Voting abgegeben!';
            } else {
              _i.title = 'Manager dieses Teams hat noch kein Voting abgegeben!';
            }
            td.append(_i);
          }

          var img = _e('img');
          img.classList = 'flags';
          img.src = '/images/flags/'+(( v[0].flag ) ? v[0].flag : v[0].nt.toLowerCase())+'.svg';
          td.append(img);

          var span = _e('span');
          span.innerText = ' ';
          td.append(span);

          var _a = _e('a');
          _a.href = "javascript:calculateTeam("+((saison == 0 && false) ? SAISON_NOW : saison)+",'"+v[0].nt+"\')";
          _a.innerHTML = v[0].name.replace(/\(NL\)/g,'<sup>'+v[0].nt.toUpperCase()+'</sup></a>');
          td.append(_a);

          var _a = _e('a');
          _a.href = v[0].link;
          _a.target = 'ultrasoccer';

          var _i = _e('i');
          _i.classList = 'fas fa-link';
          _a.append(_i);
          td.append(_a);

          if( trophy.toLowerCase() == v[0].nt.toLowerCase() ){
            var _i = _e('i');
            _i.classList = 'fas fa-trophy';
            _i.title = 'Amtierender Titelgewinner';
            td.append(_i);
          }

          if( master.toLowerCase() == v[0].nt.toLowerCase() ){
            var _i = _e('i');
            _i.classList = 'fas fa-crown';
            _i.title = 'Amtierender Meister';
            td.append(_i);
          }

          if(  v[0].nt.toLowerCase() == MANAGERVOTING_TEAM ){
            var _i = _e('i');
            _i.classList = 'managervoted fas fa-thumbs-up';
            _i.title = 'Team der Saison, gewählt von den Managern';
            td.append(_i);
          }
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = ((pokal>0) ? pokal : '');
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = ((meistertitel>0) ? meistertitel : '');
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = ((finalist>0) ? finalist : '');
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          td.innerText = ((managervoted>0) ? managervoted : '');
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[1].Tr.map( (a) => ( ( a == 'S') ? 1 : 0 ) ).reduce( (a,b) => a+b );
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[1].Tr.map( (a) => ( ( a == 'U') ? 1 : 0 ) ).reduce( (a,b) => a+b );
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[1].Tr.map( (a) => ( ( a == 'N') ? 1 : 0 ) ).reduce( (a,b) => a+b );
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          td.innerHTML = v[1].Tr.map( (a) => '<'+a+'></'+a+'>' ).slice(-6).join('');
          tr.append(td);

          var td = _e('td');
          td.classList = border;
          td.innerText = v[1].Tp+'/'+v[1].Tn;
          tr.append(td);
          
          var td = _e('td');
          td.classList = border + ' ' + ((v[1].Td > 0 ) ? 'p' : (v[1].Td == 0 ) ? 'u' : 'n');
          td.innerText = ((v[1].Td > 0 ) ? '+' : '') + v[1].Td;
          tr.append(td);
          
          var td = _e('td');
          td.classList = border + ' ' + ((v[1].Zwk > 0 ) ? 'p' : (v[1].Zwk == 0 ) ? 'u' : 'n');
          td.innerText = (v[1].Zwk > 0 ? '+' : '')+(v[1].Zwk / S).toFixed(2);
          tr.append(td);
          
          var td = _e('td');
          td.classList = border;
          td.innerText = v[1].P;
          tr.append(td);

          $(tabelle).append(tr);
        }
      );
    }
  );

}

function calculateTabelle(saison,param){
  var STATISTIK=saison == 0 ? STATISTIK_ALL : STATISTIK_SAISON;
  var n=Object.entries( STATISTIK ).sort( (a,b) => ( a[1][param] < b[1][param] || ( a[1][param] == b[1][param] && a[1].S > b[1].S ) ) ? 1 : -1 ).slice(0,20).map(
    function(v,i){
      var team=TEAMS.filter( (a) => ( a.nt == v[1].nt ) );

      var tr=document.createElement('tr');

      if( team.length == 0 ){
        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/xx.svg'
        flag.title='n/a';
        var span = document.createElement('span');
        span.innerText='';

      } else {

        var face = document.createElement('td');
        face.classList='face';

        if( v[1].face.split(/_/g).length == 5 ){
          var img = document.createElement('img');
          img.src='images/facegenerator/fg_head_' + v[1].face.split(/_/g)[0] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_hair_' + v[1].face.split(/_/g)[1] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_beard_' + v[1].face.split(/_/g)[2] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_mouth_' + v[1].face.split(/_/g)[3] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_eyes_' + v[1].face.split(/_/g)[4] + '.png';
          $(face).append(img);

        }


        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/'+(( team[0].flag ) ? team[0].flag : team[0].nt.toLowerCase())+'.svg';
        flag.title=team[0].name.split('(NL)')[0].trim();

        var span = document.createElement('span');
        var a_ = document.createElement('a');
        a_.href='javascript:calculateTeam('+saison+',\''+team[0].nt+'\')';
        a_.innerHTML=team[0].name.replace(/\(NL\)/g,'<sup>'+team[0].nt.toUpperCase()+'</sup> ');
        span.append(a_);

        var a_ = document.createElement('a');
        a_.href=team[0].link;
        a_.target='ultrasoccer';
        var i_ = document.createElement('i');
        i_.classList='fas fa-link';
        a_.append(i_);
        span.append(a_);
      }

      var attr=document.createElement('span');
      attr.innerHTML=((v[1].attr != undefined) ? v[1].attr : '').split(' ').map( (a) => ( a != '' && a != 'fas' ? '<i class="fas '+a+'"></i>' : '' ) ).join('');

      var td=document.createElement('td');
      td.innerText=(i+1);
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=(( STATISTIK[v[0]] != undefined ) ? STATISTIK[v[0]].S : 0);
      tr.append(td);

      tr.append(face);

      var td=document.createElement('td');
      td.append(flag);
      td.append(attr);

      var a_=document.createElement('a');
      a_.href='https://ultrasoccer.de/playerprofile/?id='+v[0];
      a_.target='ultrasoccer';
      a_.innerText=(( v[1].name != null ) ? v[1].name+' ('+(v[1].age*1+((saison == 0 ) ? SAISON_NOW : saison )*1-v[1].age_saison)+')' : v[0] );
      td.append(a_);
      tr.append(td);

      var td=document.createElement('td');
      td.append(span)
      tr.append(td);

      var td=document.createElement('td');
      td.innerText = ( param == 'Z' &&  v[1][param] > 0 ) ? '+' + v[1][param] :  v[1][param];
      if( param == 'Z' ) td.classList = v[1][param] > 0 ? 'p' : ( v[1][param] == 0 ) ? 'u' : 'n';
      tr.append(td);

      return tr;

    }
  );

  $('#tabelle_'+param+' tbody').html( n );
  $('.news').toggleClass('d-none', false);
}

function calculateTeam(saison,nt){
  var STATISTIK=saison == 0 ? STATISTIK_ALL : STATISTIK_SAISON;

  var n=SPIELER.filter( (a) => ( a.nt == nt ) ).map( (b) => ( [ b.id, STATISTIK[b.id] ] ) ).filter( (c) => ( c[1] !== undefined ) ).map(
    function(v,i){
      var team=TEAMS.filter( (a) => ( a.nt == v[1].nt ) );

      var tr=document.createElement('tr');

      if( team.length == 0 ){
        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/xx.svg'
        flag.title='n/a';
        var span = document.createElement('span');
        span.innerText='';

      } else {

        var face = document.createElement('td');
        face.classList='face';

        if( v[1].face.split(/_/g).length == 5 ){
          var img = document.createElement('img');
          img.src='images/facegenerator/fg_head_' + v[1].face.split(/_/g)[0] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_hair_' + v[1].face.split(/_/g)[1] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_beard_' + v[1].face.split(/_/g)[2] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_mouth_' + v[1].face.split(/_/g)[3] + '.png';
          $(face).append(img);

          var img = document.createElement('img');
          img.src='images/facegenerator/fg_eyes_' + v[1].face.split(/_/g)[4] + '.png';
          $(face).append(img);
        } else {
          face.innerHTML = '&nbsp;';
        }

        tr.append(face);

        var flag = document.createElement('img');
        flag.classList='flags';
        flag.src='./images/flags/'+(( team[0].flag ) ? team[0].flag : team[0].nt.toLowerCase())+'.svg';
        flag.title=team[0].name.split('(NL)')[0].trim();

        var span = document.createElement('span');
        var a_ = document.createElement('a');
        a_.href='javascript:calculateTeam('+saison+',\''+team[0].nt+'\')';
        a_.innerHTML=team[0].name.replace(/\(NL\)/g,'<sup>'+team[0].nt.toUpperCase()+'</sup> ');
        span.append(a_);

        var a_ = document.createElement('a');
        a_.href=team[0].link;
        a_.target='ultrasoccer';
        var i_ = document.createElement('i');
        i_.classList='fas fa-link';
        a_.append(i_);
        span.append(a_);
      }

      var attr=document.createElement('span');
      attr.innerHTML=((v[1].attr != undefined) ? v[1].attr : '').split(' ').map( (a) => ( a != '' && a != 'fas' ? '<i class="fas '+a+'"></i>' : '' ) ).join('');

      var td=document.createElement('td');
      td.append(flag);
      tr.append(td);

      var td=document.createElement('td');
      td.append(attr);
      var a_=document.createElement('a');
      a_.href='https://ultrasoccer.de/playerprofile/?id='+v[0];
      a_.target='ultrasoccer';
      a_.innerText=(( v[1].name != null ) ? v[1].name : v[0] );
      td.append(a_);
      tr.append(td);


      var td=document.createElement('td');
      td.innerText=(v[1].age*1+((saison == 0 ) ? SAISON_NOW : saison )*1-v[1].age_saison);
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=v[1].S;
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=v[1].T;
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=v[1].A;
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=v[1].Zpos+'/'+v[1].Zneg;
      td.classList=(( v[1].Z > 0 ) ? 'p' : (( v[1].Z < 0 ) ? 'n' : 'u' ) )
      tr.append(td);

      var td=document.createElement('td');
      td.innerText=' ';
      tr.append(td);

      return tr;
    }
  );
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_team').toggleClass('d-none',false);
  $('#tabelle_team tbody').html( n );
}
function calculateEwige(){
  //calculatePunktestand(0,'7');
  calculateEwigePunktestand();
  calculateTabelle(0,'T');
  calculateTabelle(0,'A');
  calculateTabelle(0,'Z');
  $('button.active').toggleClass('active',false);
  $('#ewige').toggleClass('active',true);
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_P').toggleClass('d-none');
  $('h4.tabelle > button.punktestand').toggleClass('active',true);
  $('h4.tabelle').toggleClass('d-none',false);
  $('.news').toggleClass('d-none', true);
}

function backToActiveTable(){
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_'+ $('button.active').text().toLowerCase() ).toggleClass('d-none');
}

function calculateHistorie(){
  $('#tabelle_historie thead tr').html('<th>Pl.</th>');
  var T=[];
  for( var i=0; i<=SAISON_NOW-26; i++){
    $('#tabelle_historie thead tr').append('<th>'+(i+1)+'<sup>'+(i+26)+'</sup></th>');
    T.push(calculatePunktestand(i+26,7,false));
  }
  $('#tabelle_historie thead tr').append('<th class="rest"></th>');

  var masters=ERGEBNISSE.filter((a) => ( a.runde == 'F' )).map((a) => (a.TH*1 > a.TA*1 ? a.H.toLowerCase(): a.A.toLowerCase() ));
  var mx=T.map((a)=>(a.length)).reduce((a,b) => (Math.max(a,b)));


  var tmp='';
  for( var p=0; p<mx; p++ ){
    tmp+='<tr><td>'+(p+1)+'</td>';
    for( var i=0; i<=SAISON_NOW-26; i++){
      var nt=T[i][p] != undefined ? T[i][p][0].nt : undefined;
      tmp+='<td class="'+(( MANAGERVOTING_WINNER[i+26] == nt && nt != undefined ) ? 'managervoted ' : '')+(( masters[i] == nt && nt != undefined ) ? 'master ' : '')+(( SAISON_SETTINGS[i] == p+1 ) ? 'dashed ' : '')+'">'+(( nt != undefined ) ? '<img class="flags" title="'+T[i][p][0].name+'" src="./images/flags/'+(( T[i][p][0].flag ) ? T[i][p][0].flag : T[i][p][0].nt.toLowerCase())+'.svg">' : '')+'</td>';
    }
    tmp+='</tr>';
  }
  $('#tabelle_historie tbody').html(tmp);
  $('table.tabelle').toggleClass('d-none',true);
  $('#tabelle_historie').toggleClass('d-none',false);
  $('button.active').toggleClass('active',false);
  $('#historie').toggleClass('active',true);
  $('h4.tabelle').toggleClass('d-none',true);
  $('.news').toggleClass('d-none', true);
  $('.rest')[0].scrollIntoView();
}

var R=undefined;

function showLastResults(H,A){
  $('#lastresults').toggleClass('d-none',false);
  R=ERGEBNISSE.filter((a) => ( a.H==H && a.A==A || a.A==H && a.H==A) ).slice(-5);
  $('#lastresults table tbody').html(R.map(
      function(c){
        var teamH=TEAMS.filter( (a) => ( a.nt == c.H ) );
        teamHflag = teamH[0].flag ? teamH[0].flag : teamH[0].nt;
        var teamA=TEAMS.filter( (a) => ( a.nt == c.A ) );
        teamAflag = teamA[0].flag ? teamA[0].flag : teamA[0].nt;

        var tr = _e('tr');

        var td = _e('td');
        td.innerText = c.saison;
        tr.append(td);

        var td = _e('td');
        td.innerText = c.runde;
        tr.append(td);

        var td = _e('td');
        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( c.H == H ) ? teamHflag : teamAflag )+'.svg';
        td.append(img);
        tr.append(td);

        var td = _e('td');
        var img = _e('img');
        img.classList = 'flags';
        img.src = './images/flags/'+(( c.A == A ) ? teamAflag : teamHflag )+'.svg';
        td.append(img);
        tr.append(td);

        var td = _e('td');
        var _a = _e('a');
        _a.href = 'https://ultrasoccer.de/match/?id='+c.MID;
        _a.target = 'ultrasoccer';
        _a.innerText = (( c.H == H ) ? c.TH : c.TA );
        td.append(_a);
        tr.append(td);

        var td = _e('td');
        td.innerText = ':';
        tr.append(td);

        var td = _e('td');
        var _a = _e('a');
        _a.href = 'https://ultrasoccer.de/match/?id='+c.MID;
        _a.target = 'ultrasoccer';
        _a.innerText = (( c.A == A ) ? c.TA : c.TH );
        td.append(_a);
        tr.append(td);

        return tr;
      }
    )
  );

  var d=R.map(
    (a) =>
    (
      { 'TH' : (( a.H == H ) ? parseInt(a.TH) : parseInt(a.TA)),
        'TA' : (( a.A == A ) ? parseInt(a.TA) : parseInt(a.TH)),
        'S' : (( a.H == H ) ? parseInt(a.TH) > parseInt(a.TA) : parseInt(a.TA) > parseInt(a.TH)) ? 1 : 0,
        'U' : parseInt(a.TH) == parseInt(a.TA) ? 1 : 0,
        'N' : (( a.H == H ) ? parseInt(a.TH) < parseInt(a.TA) : parseInt(a.TA) < parseInt(a.TH)) ? 1 : 0
      }
    )
  ).reduce(
    (sum,val) =>
    (
      sum.TH += val.TH,
      sum.TA += val.TA,
      sum.S  += val.S,
      sum.U  += val.U,
      sum.N  += val.N,
      sum
    )
  );

  var tr = _e('tr');
  tr.classList = 'lastresults_summe';

  var td = _e('td');
  td.colspan = 4;
  td.innerText = 'Tore';
  tr.append(td);

  var td = _e('td');
  td.innerText = d.TH;
  tr.append(td);

  var td = _e('td');
  td.innerText = ':';
  tr.append(td);

  var td = _e('td');
  td.innerText = d.TA;
  tr.append(td);

  $('#lastresults table tbody').append(tr);

  var tr = _e('tr');
  tr.classList = 'lastresults_summe';

  var td = _e('td');
  td.colspan = 4;
  td.innerText = 'S/U/N';
  tr.append(td);

  var td = _e('td');
  td.innerText = d.S;
  tr.append(td);

  var td = _e('td');
  td.innerText = d.U;
  tr.append(td);

  var td = _e('td');
  td.innerText = d.N;
  tr.append(td);

  $('#lastresults table tbody').append(tr);

}

var ignore_hidelastresults=false;

function hideLastResults(force=false){
  if( !ignore_hidelastresults || force ) $('#lastresults').toggleClass('d-none',true);
  if( force ) ignore_hidelastresults=false;
}

function ignoreHideLastResults(){
  ignore_hidelastresults=true;
}


function calculateAllPunktestand(){

  var T=[];
  for( var i=0; i<=SAISON_NOW-26; i++){
    if( ERGEBNISSE.filter((a) => ( a.runde=='F' && a.saison==i+26 )).length == 1 ){
      T.push( JSON.parse(JSON.stringify( calculatePunktestand(i+26,7,false) )) );
    }
  }
  return T;
}

function teamVoting(nt){
  var team=TEAMS.filter((a) => (a.nt == nt && a.ende == 0))[0];
  var cd=prompt('TeamVote für \n\n' + team.name + '\n\n -> Bitte AuthCode eingeben!');
  $.ajax({
    'type':"POST",
    'async': false,
    'url': 'vote.php',
    'data' : { 'teamvote' : nt, 'secret' : cd },
    'dataType': 'json',
    'success': function (data) {
        json = data;
    },
    'error': function (data) {
      json = data;
    }
  });
  if( json[3] ){
    alert( 'Teamvoting erfolgreich gesetzt!' );
  } else {
    console.log( json );
  }
}