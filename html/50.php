---
layout: default
saison: 50
date: '2024-01-19'
ligaspieltag: 7
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: tz  # tz 3 / pl 2 / fr nl de 1
startliste:
  -
    - fi
    - kz
    - se
    - tz
    - pl
    - nl
    - co
    - is
    - de
    - fr
    - us

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
