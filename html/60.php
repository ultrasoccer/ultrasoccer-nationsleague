---
layout: default
saison: 60
date: '2024-12-29'
ligaspieltag: 2
step: 1
finals_step: 1
finalteams: 8
managervoting: 
  enabled: false
  result: bo # bo 4 ca 3 au ar co 1 
startliste:
  -
    - se
    - kz
    - is
    - pl 
    - fo
    - nl
    - fr
    - co
    - kr
    - tz
    - ca
    - gh
    - au
    - ar
    - bo
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 1
  - 1
  - 1
---
