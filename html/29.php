---
layout: default
saison: 29
date: '2022-01-15'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 4
startliste:
  -
    - co
    - hr
    - is
    - fr
    - bw
    - hu
    - pl
    - cm
    - es

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 4
  - 2
  - 3
---
