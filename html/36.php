---
layout: default
saison: 36
date: '2022-09-16'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - at
    - se
    - es
    - is
    - bw
    - pl
    - nl
    - co
    - cm

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
