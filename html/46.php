---
layout: default
saison: 46
date: '2023-08-30'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: nl  # nl 5 / de 1 / eine Enthaltung
startliste:
  -
    - kz
    - jp
    - co
    - pl
    - is
    - fr
    - nl
    - de
    - se
    - bw
    - tz

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
---
