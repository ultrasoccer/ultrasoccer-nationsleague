---
layout: default
saison: 56
date: '2024-08-13'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: ca # ca 5 / kr 2 / tz fo 1
startliste:
  -
    - kz
    - fo
    - us
    - pl
    - se 
    - nl 
    - ca 
    - co
    - gh
    - tz
    - is
    - fr
    - kr
    - au
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 0
---
