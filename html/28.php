---
layout: default
saison: 28
date: '2021-12-11'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 4
startliste:
  -
    - cz
    - fr
    - co
    - is
    - bw
    - hu
    - it
    - pl
    - cm
    - hr
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 4
  - 2
  - 3
---
