---
layout: default
saison: 53
date: '2024-04-30'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: us # 6 us / 1 de kz
startliste:
  -
    - fr
    - de
    - nl
    - tz 
    - se 
    - co 
    - pl
    - is
    - us
    - kr
    - kz
    - fi
    - ie
    - fo
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 3
  - 1 
  - 2
  - 0
---
