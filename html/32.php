---
layout: default
saison: 32
date: '2022-04-28'
ligaspieltag: 7
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - es
    - fi
    - se
    - is
    - co
    - cm
    - fr
    - bw
    - pl
    - hr
    - sx
    - nl

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
