---
layout: default
saison: 31
date: '2022-03-26'
ligaspieltag: 8
step: 1
finals_step: 2
finalteams: 4
startliste:
  -
    - se
    - es
    - is
    - hr
    - cm
    - pl
    - bw
    - co
    - hu
    - fr
    - fi

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
---
