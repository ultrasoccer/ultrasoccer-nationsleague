---
layout: default
saison: 35
date: '2022-08-15'
ligaspieltag: 11
step: 1
finals_step: 2
finalteams: 8
startliste:
  -
    - bw
    - es
    - is
    - nl
    - se
    - co
    - cm
    - pl
    - hr
    - at

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
  - 0
---
