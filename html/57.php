---
layout: default
saison: 57
date: '2024-09-17'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: au # au ca 3 / kr 2 / tz 1 = Stichwahl au 5 / ca 3
startliste:
  -
    - se
    - kz
    - is
    - nl
    - au 
    - kr 
    - gh
    - co
    - tz
    - fo
    - fr
    - ca
    - pl
    - en
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 1
  - 2
  - 2
  - 2 
  - 2
  - 0
---
