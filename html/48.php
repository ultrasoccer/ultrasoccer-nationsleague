---
layout: default
saison: 48
date: '2023-11-08'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: de   # de 4 / nl fi bw fr 1
startliste:
  -
    - kz
    - bw
    - co
    - pl
    - se
    - is
    - fr
    - jp
    - tz
    - de
    - nl
    - fi

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 0
---
