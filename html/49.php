---
layout: default
saison: 49
date: '2023-12-11'
ligaspieltag: 3
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: fr # fr 5 / tz 2
startliste:
  -
    - fi
    - kz
    - pl
    - tz
    - nl
    - se
    - is
    - bw
    - fr
    - co
    - de

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 4
  - 2
  - 4
  - 2
  - 0
---
