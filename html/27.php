---
layout: default
saison: 27
date: '2021-11-16'
ligaspieltag: 18
step: 1
finals_step: 2
finalteams: 4
startliste:
  -
    - bw
    - cm
    - co
    - cz
    - fr
    - hu
    - is
    - it
    - pl
startliste_partien_pause:
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 1
  - 2
  - 0
---
