---
layout: default
saison: 43
date: '2023-05-17'
ligaspieltag: 5
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: is  # is = 5, nl = 2
startliste:
  -
    - jp
    - bw
    - fr
    - nl
    - is
    - pl
    - de
    - se
    - tz
    - co

startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 3
  - 2
  - 2
  - 2
  - 0
---
