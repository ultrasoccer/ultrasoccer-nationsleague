---
layout: default
saison: 52
date: '2024-03-26'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: pl # pl 3 / tz 2 / nl se fr is 1
startliste:
  -
    - ie
    - kz 
    - fi 
    - us 
    - nl 
    - fr 
    - co
    - is
    - pl
    - tz
    - se
    - de
    - kr
startliste_partien_pause:
  - 2
  - 2
  - 3
  - 1
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 0
---
