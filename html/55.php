---
layout: default
saison: 55
date: '2024-07-09'
ligaspieltag: 4
step: 1
finals_step: 2
finalteams: 8
managervoting: 
  enabled: false
  result: se # 7 - se / 1 - ca
startliste:
  -
    - ca
    - se
    - kz
    - nl
    - fo 
    - is 
    - pl 
    - gh
    - us
    - co
    - tz
    - fr
startliste_partien_pause:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2 
  - 2
  - 0
---
